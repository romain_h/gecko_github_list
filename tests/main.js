/* global jasmine, htmlReporter, env*/
define(function(require) {
  'use strict';

  var customTest = require('customBoot');
  var app = require('app');
  require('spec/main');


  app.addRegions({
    headerRegion: '#grHeader',
    contentRegion: '#grContent',
    footerRegion: '#grFooter',
  });

  // Run customTest
  customTest.executeCustomTest();

});

