define(function(require) {
  'use strict';

  require('app');
  var reposJson = require('fixtures/repositories');
  var RepositoriesCollection = require('repositories/collections/repositories');

  describe("A repositories collection", function() {

    beforeEach(function() {
      this.server = sinon.fakeServer.create();
    });

    afterEach(function(){
      this.server.restore();
    });

    it("should return the user's url", function() {
      var opts = { username: 'john-doe' };
      var collection = new RepositoriesCollection({}, opts);
      var expectedUrl = 'https://api.github.com/users/john-doe/repos?per_page=100';

      expect(collection.url).toEqual(expectedUrl);
    });

    it("should connect to the Github API", function() {
      var opts = { username: 'cjohansen' };
      var collection = new RepositoriesCollection({}, opts);

      // fake API answer with fixtures
      this.server.respondWith(
              'GET',
              collection.url,
              [
                200,
                { "Content-Type": "application/json" },
                JSON.stringify(reposJson)
              ]
      );

      collection.fetch();
      this.server.respond();

      // test collection length
      expect(collection.length).toEqual(30);

      // test result
      expect(JSON.stringify(collection.at(0))).toEqual(JSON.stringify(reposJson[29]));
    });

    it("should order models by stars", function() {
      var opts = { username: 'cjohansen' };
      var repo1, repo2, repo3;
      repo1 = new Backbone.Model({
        id: 1,
        stargazers_count: 1
      });
      repo2 = new Backbone.Model({
        id: 2,
        stargazers_count: 2
      });
      repo3= new Backbone.Model({
        id: 3,
        stargazers_count: 3
      });

      var collection = new RepositoriesCollection([repo1, repo2, repo3], opts);

      expect(collection.at(0)).toBe(repo3);
      expect(collection.at(1)).toBe(repo2);
      expect(collection.at(2)).toBe(repo1);
    });
  });
});
