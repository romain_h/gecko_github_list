# Geckoboard Challenge Frontend
> Simple application to show a list of the public repositories from a Github
> user

A demo is available: [http://geckoboard.hrdy.me](http://geckoboard.hrdy.me)

## Requirements

+ Nodejs 0.10.x
+ Gulp 3.8.x
+ Bower 3.8.x

## Installation
First you need to install [nodejs](http://nodejs.org/). On Macosx you can use
brew:

```
$ brew install node
```

This project use [Gulp.js](http://gulpjs.com/) as build system. You need to have
it globally.

```
$ sudo npm install -g gulp
```

And require [Bower](http://bower.io/) as package manager.
```
$ sudo npm install -g bower
```

### Github token
In order to not be limited to 60 requests per hour, you need an API token from
Github. Go in [your applications settings](https://github.com/settings/applications)
 and generate a new token with the following scope:

 + *public_repos*
 + *user*

### Project configuration
Once this project has been cloned run the following:

```
$ cd <gecko_github_list>
$ npm install
$ bower install
```

Then setup your local configuration file with:
```
$ cp app/settings/local.json.default app/settings/local.json
```

You can now edit your local file to add you Github token and enter the username
of a Githuber to list his public repositories!

```
$ vim app/settings/local.json # To edit token and username
```

## Running the project
To run the project use this command:
```
$ cd <gecko_github_list>
$ gulp
```

The application is now ready on [http://localhost:8765](http://localhost:8765)


## Run tests
This project use Jasmine.js and Sinon.js for unit tests.

You can run tests by browsing [http://localhost:8765/test.html](http://localhost:8765/test.html)

This application is not fully covered yet.

## Deployment

A build task is available for this project. You can use the `app/settings/production.json`
settings file to use a different username or token.

Then run this command to build the project:
```
$ gulp build --env production
```

All the files inside `<gecko_github_list>/dist` are ready to be deployed on
a server or a CDN.

### To do

+ Move the tests environment from the developement environment
