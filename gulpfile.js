/* jshint strict:false, node:true */
/* global require  */

var args = require('yargs').argv;
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var defineModule = require('gulp-define-module');
var fs = require('fs');
var gulp = require('gulp');
var handlebars = require('gulp-handlebars');
var historyApiFallback = require('connect-history-api-fallback');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var path = require('path');
var requirejs = require('requirejs');
var template = require('gulp-template');
var uglify = require('gulp-uglify');

// Get environment
var isProduction = args.env === 'production';

// Server task
gulp.task('connect', function() {
  connect.server({
    port: 8765,
    livereload: true,

    middleware: function(connect, opt) {
      return [ historyApiFallback ];
    }
  });
});

// Generate settings files
gulp.task('generate-settings', function() {
  var settingsFile;
  if (isProduction) {
    settingsFile = './app/settings/production.json';
  } else {
    settingsFile = './app/settings/local.json';
  }

  // Get settings string
  var settingsString = fs.readFileSync(settingsFile, "utf8");
  var settings = JSON.parse(settingsString);
  var data = {
    settings: JSON.stringify(settings)
  };

  // Inject in settings file that will be required
  gulp.src(['./app/settings/settings.js'])
  .pipe(template( data ))
  .pipe(gulp.dest('./tmp/'));

});

// Compile less files
gulp.task('less', function () {
  gulp.src('./assets/stylesheets/less/main.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'assest/stylesheets', 'less') ]
    }))
    .pipe(gulp.dest('./assets/stylesheets'));
});

// Compile handlebars templates
gulp.task('templates', function() {
  gulp.src(['app/templates/**/*.html'])
    .pipe(handlebars())
    .pipe(defineModule('amd'))
    .pipe(gulp.dest('app/templates/precompiled/templates'));
});

gulp.task('lint', function() {
    // Concat result in one file
     gulp.src('./app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));

});

// Default task to run without params
gulp.task('default', ['connect', 'generate-settings', 'less', 'templates'], function() {
  // Watch less files and compile
  gulp.watch('./assets/stylesheets/less/**/*.less', ['less']);

  // Watch handlebars templates files
  gulp.watch('./app/templates/**/*.html', ['templates']);
});


gulp.task('build', ['generate-settings', 'lint', 'less', 'templates'], function() {
  // Copy index.html template
  gulp.src('./app/templates/index.html')
  .pipe(gulp.dest('./dist/'));

  // Copy main.css
  gulp.src('./assets/stylesheets/main.css')
  .pipe(gulp.dest('./dist/'));

  // Copy special fonts
  gulp.src(['./assets/stylesheets/octicons/**/*.*'])
  .pipe(gulp.dest('./dist/octicons'));

  requirejs.optimize({
    dir: 'build',
    baseUrl: 'app/',
    removeCombined: true,
    mainConfigFile: './app/config.js',
    modules: [{ "name": "config" }],
    optimize: 'none'
    });

    // Concat result in one file
     gulp.src(['bower_components/requirejs/require.js','build/config.js'])
      .pipe(uglify())
      .pipe(concat('main.js'))
      .pipe(gulp.dest('./dist/'));

});
