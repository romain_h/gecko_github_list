define(function(require) {
  'use strict';

  var app = require('app');
  require('utils/backbone_sync');
  require('utils/helpers/templates');
  var loader = require('utils/progressbar');

  // Init routers
  require('repositories/router');

  app.addInitializer(function() {
    Backbone.history.start({ pushState: true });
  });

  // Main entry point when document is ready
  $(document).ready(function () {
    // Define app region
    app.addRegions({
      headerRegion: '#grHeader',
      userRegion: '#grHeader .userHeader',
      contentRegion: '#grContent',
      footerRegion: '#grFooter',
    });

    // Init Global loader
    app.loader = loader;

    app.start();
  });
});
