// Set the require.js configuration for your application.
/* global require */
require.config({
  // Initialize the application with the main application file
  deps: ['main'],

  paths: {
    // Libraries
    backbone: '../bower_components/backbone/backbone',
    backboneMarionette: '../bower_components/marionette/lib/core/backbone.marionette',
    'backbone.wreqr': '../bower_components/backbone.wreqr/lib/backbone.wreqr',
    'backbone.babysitter': '../bower_components/backbone.babysitter/lib/backbone.babysitter',
    handlebars: '../vendor/js/handlebars.runtime',
    jquery: '../bower_components/jquery/dist/jquery',
    jqueryTimeago: '../bower_components/jquery-timeago/jquery.timeago',
    nprogress: '../bower_components/nprogress/nprogress',
    templates: 'templates/precompiled/templates',
    underscore: '../bower_components/underscore/underscore',
    settings: '../tmp/settings',
  },

  shim: {
    backbone: {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    backboneMarionette: {
      deps: ['backbone'],
      exports: 'Marionette'
    },
    handlebars: {
      exports: 'Handlebars'
    },
    jquery: {
      exports: 'jQuery'
    },
    jqueryTimeago: {
      deps: ['jquery'],
    },
    nprogress: {
      exports: 'nprogress'
    },
    templates: {
      deps: ['handlebars']
    }
  }
});
