define(function(require) {
  'use strict';

  var Handlebars = require('handlebars');

  Handlebars.registerHelper('dateTimeago', function (time) {
    var ret = '<abbr class="timeago modified_on" title="' + time + '"></abbr>';

    return new Handlebars.SafeString(ret);
  });
});
