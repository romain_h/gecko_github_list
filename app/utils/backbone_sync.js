define(function(require) {
  'use strict';

  var app = require('app');

  /*
   * Modifies Backbone.sync to include an Authorization header.
   * since the unAuthorized Github API is limited to 60 request per hour
   * TODO: Move the key in a config file and untrack it
   */
  Backbone.originalSync = Backbone.sync;

  /**
   * Override 'Backbone.sync'
   */
  Backbone.sync = function(method, model, options) {
    options = options || {};

     var customHeaders = {
      'Authorization': 'token ' + app.settings.tokenGithub,
    };

    // Loader indicator handler
    app.loader.start();

    var originalSuccess = options.success;
    var originalError = options.error;

    /* jshint unused: false */
    options.success = function(resp, textStatus, jqXHR) {
      originalSuccess(resp);
      app.loader.done();
    };

    options.error = function(jqXHR, textStatus, errorThrown) {
      app.loader.done();
      originalError(jqXHR);
    };
    /* jshint unused: true */

    options.headers = _.extend(customHeaders, options.headers);

    // use the default sync methode
    return Backbone.originalSync.call(this, method, model, options);
  };

});

