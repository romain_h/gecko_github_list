define(function(require) {
  'use strict';

  var app = require('app');

  var Repository = Backbone.Model.extend({
    url: 'https://api.github.com/repos/{user}/{repo}',

    initialize: function(options) {
      options = options || {};

      if (!options.url){
        if (app.settings.username) {
          this.url = this.url.replace('{user}', app.settings.username);
        }
        if (options.name) {
          this.url = this.url.replace('{repo}', options.name);
        }
      }
    },
  });

  return Repository;
});
