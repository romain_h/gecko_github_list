define(function(require){
  'use strict';

  var app = require('app');
  var controller = require('repositories/controller');

  var routing = {};

  routing.Router = Backbone.Marionette.AppRouter.extend({
    appRoutes: {
      '': 'init',
      'repository/:id/': 'repoDetail',
    },
    controller: controller
  });

  app.addInitializer(function() {
    routing.router = new routing.Router();
  });

  return routing;
});
