define(function(require) {
  'use strict';

  require('app');
  var template = require('templates/repositories/views/repository_list_elt');

  var View = Backbone.Marionette.ItemView.extend({
    className: 'repoElt list-group-item',
    tagName: 'li',

    events: {
      'click .title': 'navigate',
    },

    template: template,

    navigate: function(evt){
      evt.preventDefault();

      if (this.model.get('name')) {
        var url = 'repository/' + this.model.get('name') + '/';
        Backbone.history.navigate(url, { trigger: true });
      }
    },
  });

  return View;
});
