define(function(require) {
  'use strict';

  require('jqueryTimeago');
  require('app');
  var template = require('templates/repositories/views/repository_detail');

  var View = Backbone.Marionette.ItemView.extend({
    className: 'repoDetail',
    template: template,

    events: {
      'click #back_button': 'navigateBack'
    },

    navigateBack: function(evt) {
      evt.preventDefault();

      Backbone.history.navigate('/', { trigger: true });
    },
    onRender: function() {
      this.$el.find('.modified_on').timeago();
    }
  });

  return View;
});
