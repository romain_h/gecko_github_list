define(function(require) {
  'use strict';

  var template = require('templates/repositories/views/repositories_collection');
  var RepositoryListView = require('repositories/views/repository_list');

  var View = Backbone.Marionette.CollectionView.extend({
    template: template,
    childView: RepositoryListView,
    className: 'geckoMainList list-group',
    tagName: 'ul',
    limit: 20,

    /**
     * showCollection - override internal method to display limited collection
     * @return null
     */
    showCollection: function() {
      var ChildView;
      var limitedCol = this.collection.first(this.limit);
      _.each(limitedCol, function(child, index) {
        ChildView = this.getChildView(child);
        this.addChild(child, ChildView, index);
      }, this);
    },

  });

  return View;
});
