define(function(require) {
  'use strict';

  var app = require('app');
  var RepoModel = require('repositories/models/repository');
  var RepositoriesCollection = require('repositories/collections/repositories');
  var ReposCollectionView = require('repositories/views/repositories_collection');
  var RepoDetailView = require('repositories/views/repository_detail');
  var User = require('users/models/user');
  var UserView = require('users/views/user');

  var controller = {};

  controller.header = function() {
    // store main user
    if (!app.user) {
      app.user = new User({ username: app.settings.username });

      // Display user header on fetched done
      app.user.fetch().done(function() {
        var userView = new UserView({ model: app.user });
        app.userRegion.show(userView);
      });
    }
  };

  controller.init = function() {
    this.header();

    // Init repo collection
    var collection = new RepositoriesCollection({}, { username: app.settings.username });
    collection.fetch();

    var reposView = new ReposCollectionView({ collection: collection });
    app.contentRegion.show(reposView);
  };

  // Detailed repository information
  controller.repoDetail = function(name) {
    this.header();

    var repo = new RepoModel({ name: name });

    var detailView = new RepoDetailView({
      model: repo,
    });

    // On fetch callback show the detailled view on the content region
    repo.fetch().done(function() {
      app.contentRegion.show(detailView);
    });
  };

  return controller;

});
