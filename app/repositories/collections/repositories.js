define(function(require) {
  'use strict';

  require('app');
  var Repository = require('repositories/models/repository');

  var Repositories = Backbone.Collection.extend({
    url: 'https://api.github.com/users/{user}/repos?per_page=100',
    model: Repository,

    initialize: function(models, options){
      options = options || {};

      // Replace username:
      if (options.username) {
        this.url = this.url.replace('{user}', options.username);
      }

      // Set comparator on stars/watchers
      this.comparator = function(model) {
        return -model.get('stargazers_count');
      };
    },

    /*
    * parse_link_header()
    *
    * Parse the Github Link HTTP header used for pagination
    * http://developer.github.com/v3/#pagination
    *
    * From Gist: https://gist.github.com/niallo/3109252
    */
    parse_link_header: function(header) {
      if (header.length === 0) {
        throw new Error("input must not be of zero length");
      }

      // Split parts by comma
      var parts = header.split(',');
      var links = {};
      // Parse each part into a named link
      _.each(parts, function(p) {
        var section = p.split(';');
        if (section.length !== 2) {
          throw new Error("section could not be split on ';'");
        }
        var url = section[0].replace(/<(.*)>/, '$1').trim();
        var name = section[1].replace(/rel="(.*)"/, '$1').trim();
        links[name] = url;
      });

      return links;
    },

    /**
     * Override Fetch methode to handle pagination if needed
     * to batch requests
     * @return jQuery.promise()
     */
    fetch: function(options) {
      var that = this;

      var mainDeferred = $.Deferred();
      var jqXHR = Backbone.Collection.prototype.fetch.call(this, options);
      jqXHR.done(function() {
        var headers = jqXHR.getResponseHeader('Link');

        // Do we need to handle pagination?
        if (headers) {
          var linksPagination = that.parse_link_header(headers);

          // Get last page number
          var reg = /&page=(\d+)/;
          var lastPageInt = reg.exec(linksPagination.last);
          that.nbPages = lastPageInt[1];

          // Batch the fetch of all next page in a promise
          var nextPagesPromise = that.fetchNexts(options);

          nextPagesPromise.done(function() {
            mainDeferred.resolve();
          });
        } else {
          // resolve if no more page
          mainDeferred.resolve();
        }
      });

      return mainDeferred.promise();
    },

    /**
     * Fetch all next pages with promises
     * to batch requests
     * @return jQuery.promise()
     */
    fetchNexts: function(options) {
      options = options || {};

      var mainDeferred = $.Deferred();
      var promises = [];

      // create promises for all pages
      var i;
      var n;
      for (i = 1, n = this.nbPages; i < n; ++i) {
        var opts = _.clone(options);
        // Add page parameters
        _.extend(opts, {data: $.param({ page: i + 1 })});

        // make sure that models are not removed from the collection;
        opts.remove = false;

        var jqXHR = Backbone.Collection.prototype.fetch.call(this, opts);
        promises.push(jqXHR);
      }

      $.when.apply($, promises).done(function() {
        mainDeferred.resolve();
      }).fail(function() {
        mainDeferred.reject();
      });

      return mainDeferred.promise();
    }
  });

  return Repositories;
});
