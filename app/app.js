/* global console */
define(function(require){
  'use strict';

  require('backbone');
  require('backboneMarionette');
  var settings = require('settings');

  var app = new Backbone.Marionette.Application();
  app.settings = settings;
  try {
    if (app.settings.username === undefined) {
      throw 'Please setup a username in your config file';
    }
    if (app.settings.tokenGithub === undefined) {
      throw 'Please setup a token in your config file';
    }
  } catch( err ) {
    console.log(err);
  }

  return app;
});
