define(function(require) {
  'use strict';

  require('app');

  var User = Backbone.Model.extend({
    url: 'https://api.github.com/users/{user}',

    initialize: function(options){
      options = options || {};

      // Replace username:
      if (options.username) {
        this.url = this.url.replace('{user}', options.username);
      }
    },
  });

  return User;
});
