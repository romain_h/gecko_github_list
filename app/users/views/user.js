define(function(require) {
  'use strict';

  require('app');
  var template = require('templates/users/views/user');

  var View = Backbone.Marionette.ItemView.extend({
    template: template,
  });

  return View;
});
